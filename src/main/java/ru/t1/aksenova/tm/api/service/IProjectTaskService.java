package ru.t1.aksenova.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskToProject(String projectId, String taskId);

    void removeTaskToProject(String projectId);

}
