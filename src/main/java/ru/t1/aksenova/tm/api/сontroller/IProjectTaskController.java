package ru.t1.aksenova.tm.api.сontroller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
